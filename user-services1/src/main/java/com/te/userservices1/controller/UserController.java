package com.te.userservices1.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.te.userservices1.entity.User;
import com.te.userservices1.service.UserService;

@RestController
@RequestMapping("/user")
public class UserController {
	@Autowired
	private UserService userService;
	@Autowired
	private RestTemplate restTemplate;

	@GetMapping("/{userId}")
	public User getUser(@PathVariable Integer userId) {
		 User user = userService.getUser(userId);
		 List list = restTemplate.getForObject("http://USER-CONTACTS/contact/"+userId, List.class);
		 user.setContacts(list);
		 return user;
	}
}
