package com.te.userservices1.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.ManyToOne;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Contacts {
	@Id
	private Integer cId;
	private String email;
	private String contactName;
	
	@ManyToOne
	private User user;
}
