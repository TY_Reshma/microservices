package com.te.userservices1.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.te.userservices1.entity.User;
import com.te.userservices1.repository.UserRepository;


@Service
public class UserService {

	@Autowired
	private UserRepository userRepository;

	public User getUser(Integer id) {
		return userRepository.findById(id).get();

	}

}
